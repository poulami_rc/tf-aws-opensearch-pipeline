from opensearchpy import OpenSearch, RequestsHttpConnection, AWSV4SignerAuth
import boto3
import os

host = os.environ['OPENSEARCH_HOST'] # serverless collection endpoint, without https://
region = boto3.Session().region_name
service = 'es'
credentials = boto3.Session().get_credentials()
auth = AWSV4SignerAuth(credentials, region, service)

client = OpenSearch(
    hosts = [{'host': host, 'port': 443}],
    http_auth = auth,
    use_ssl = True,
    verify_certs = True,
    connection_class = RequestsHttpConnection
)

s3 = boto3.client('s3')

def lambda_handler(event, context):
    bucket= event['Records'][0]['s3']['bucket']['name']
    key=event['Records'][0]['s3']['object']['key']
    print("bucket=", bucket)
    print("key=", key)
    obj = s3.get_object(Bucket=bucket, Key=key)
    data = obj['Body'].read()
    response=client.bulk(data)
    print(response)
    
    return {
        'statusCode': 201
    }
