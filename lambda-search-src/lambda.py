from opensearchpy import OpenSearch, RequestsHttpConnection, AWSV4SignerAuth
import boto3
import os
import json

host = os.environ['OPENSEARCH_HOST'] # serverless collection endpoint, without https://
region = boto3.Session().region_name
service = 'es'
credentials = boto3.Session().get_credentials()
auth = AWSV4SignerAuth(credentials, region, service)

client = OpenSearch(
    hosts = [{'host': host, 'port': 443}],
    http_auth = auth,
    use_ssl = True,
    verify_certs = True,
    connection_class = RequestsHttpConnection
)

def lambda_handler(event, context):
    q = None
    my_index = None
    if 'queryStringParameters' in event:
        queryStr = event['queryStringParameters']
        if 'q' in queryStr: 
            q = queryStr['q']
    
        if 'index' in queryStr: 
            my_index = queryStr['index']
            
    
    if q is None or my_index is None:
        return {
            'statusCode': 404,
            'body': 'either index or query is missing'
        }
    else:
        query = {
            'size': 5,
            'query': {
                'multi_match': {
                    'query': q
                }
            }
        }

        response = client.search(
            body = query,
            index = my_index
        )
        print(response)

        return {
            'statusCode': 200,
            'body': json.dumps(response)
        }
