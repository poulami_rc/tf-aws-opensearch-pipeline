data "aws_iam_policy_document" "data_access_policy" {
  statement {
    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = [
        aws_iam_role.iam_for_lambda_opensearch.arn
      ]
    }

    actions   = ["es:*"]
    resources = ["arn:aws:es:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:domain/tf-opensearch-domain/*"]
  }
}

resource "aws_opensearch_domain" "domain" {
  domain_name    = "tf-opensearch-domain"
  engine_version = "OpenSearch_2.11"

  cluster_config {
    instance_type = "t3.small.search"
    dedicated_master_enabled = false
    instance_count = 1
    multi_az_with_standby_enabled = false
  }
  
  advanced_security_options {
    enabled = false
  }

  encrypt_at_rest {
    enabled = true
  }
  
  ebs_options {
    ebs_enabled = true
    volume_size = 10
  }

  access_policies = data.aws_iam_policy_document.data_access_policy.json    
}