#common
resource "random_string" "r" {
  length  = 16
  special = false
}

# lambda function for data ingestion to opensearch domain
data "archive_file" "lambda_ingest_zip" {
    type          = "zip"
    source_dir    = "${path.module}/lambda-ingest-src"
    output_path   = "${path.module}/lambda-ingest-function.zip"
    depends_on = [random_string.r]
}

resource "aws_lambda_function" "data_ingest" {
  filename         = "lambda-ingest-function.zip"
  function_name    = "tf-opensearch-data-ingest"
  role             = "${aws_iam_role.iam_for_lambda_opensearch.arn}"
  handler          = "lambda.lambda_handler"
  source_code_hash = "${data.archive_file.lambda_ingest_zip.output_base64sha256}"
  runtime          = "python3.10"
  timeout          = 30  

  environment {
    variables = {
      OPENSEARCH_HOST = "${aws_opensearch_domain.domain.endpoint}"
    }
  }
}

resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.data_ingest.arn
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.s3_bucket.arn
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.s3_bucket.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.data_ingest.arn
    events              = ["s3:ObjectCreated:*"]
  }

  depends_on = [aws_lambda_permission.allow_bucket]
}

# lambda function for data search/query opensearch domain
data "archive_file" "lambda_search_zip" {
    type          = "zip"
    source_dir    = "${path.module}/lambda-search-src"
    output_path   = "${path.module}/lambda-search-function.zip"
    depends_on = [random_string.r]
}

resource "aws_lambda_function" "data_search" {
  filename         = "lambda-search-function.zip"
  function_name    = "tf-opensearch-data-search"
  role             = "${aws_iam_role.iam_for_lambda_opensearch.arn}"
  handler          = "lambda.lambda_handler"
  source_code_hash = "${data.archive_file.lambda_ingest_zip.output_base64sha256}"
  runtime          = "python3.10"
  timeout          = 30

  environment {
    variables = {
      OPENSEARCH_HOST = "${aws_opensearch_domain.domain.endpoint}"
    }
  }
}