terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }

  backend "http" {
  }
}

provider "aws" {
  region = "us-east-1"
}

data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

#s3 bucket
resource "aws_s3_bucket" "s3_bucket" {
  bucket = "tf-poulami-opensearch-data"
  force_destroy = true
}

#define output-------------------------------------------------
output "apiGatewayURL" {
    value = "${aws_api_gateway_deployment.deployment.invoke_url}${aws_api_gateway_resource.resource.path}"
}