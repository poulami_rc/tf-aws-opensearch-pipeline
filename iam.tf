data "aws_iam_policy" "lambda_basic_execution_policy" {
  arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

data "aws_iam_policy" "opensearch_full_access_policy" {
  arn = "arn:aws:iam::aws:policy/AmazonOpenSearchServiceFullAccess"
}

resource "aws_iam_role" "iam_for_lambda_opensearch" {
  name = "tf-lambda-opensearch-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "lambda_opensearch_basic_execution_policy_attach" {
  role       = aws_iam_role.iam_for_lambda_opensearch.id
  policy_arn = data.aws_iam_policy.lambda_basic_execution_policy.arn
}

resource "aws_iam_role_policy_attachment" "iam_for_lambda_opensearch_full_access_policy_attach" {
  role       = aws_iam_role.iam_for_lambda_opensearch.id
  policy_arn = data.aws_iam_policy.opensearch_full_access_policy.arn
}

data "aws_iam_policy" "s3_readonly_policy" {
  arn = "arn:aws:iam::aws:policy/AmazonS3ReadOnlyAccess"
}

resource "aws_iam_role_policy_attachment" "lambda_opensearch_s3_readonly_policy_attach" {
  role       = aws_iam_role.iam_for_lambda_opensearch.id
  policy_arn = data.aws_iam_policy.s3_readonly_policy.arn
}